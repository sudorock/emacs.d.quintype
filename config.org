* Package-Archives
#+BEGIN_SRC emacs-lisp :results silent
(require 'package)
(setq package-enable-at-startup nil)

;;add more repositories to fetch-archive list
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/"))

;;load and activate emacs lisp packages
(package-initialize)

(when (not package-archive-contents)
 (package-refresh-contents))
#+END_SRC

* Use-Package
#+BEGIN_SRC emacs-lisp :results silent
(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(eval-when-compile
  (require 'use-package))

(require 'use-package)
(setq use-package-verbose t)
(setq-default use-package-always-ensure t)
#+END_SRC

* Exec Path from Shell
#+BEGIN_SRC emacs-lisp :results silent
  (use-package exec-path-from-shell
    :ensure t
    :if (memq window-system '(mac ns))
    :defer 0.5
    :init
    (setq exec-path-from-shell-arguments '("-l")))
#+END_SRC

* Interface Tweaks
  #+BEGIN_SRC emacs-lisp :results silent
    ;;hide the start-up message
    (setq inhibit-startup-message t)

    ;;open emacs in full screen
    (add-to-list 'default-frame-alist '(fullscreen . maximized))
    ;;---------------------

    ;;disable menu-bar
    (menu-bar-mode -1)

    ;;disable tool-bar
    (tool-bar-mode -1)

    ;;disable scroll-bar
    (scroll-bar-mode -1)

    ;;-----------------------
    ;;get rid of the annoying bell
    (setq ring-bell-function 'ignore)

    ;;show line-numbers
    (global-display-line-numbers-mode t)

    ;;line wrap long lines
    (global-visual-line-mode 1)

    ;;highlight current-line
    (global-hl-line-mode 1)

    ;;no double space sentences
    (setq-default sentence-end-double-space nil)

    ;;no dialog boxes
    (setq use-dialog-box nil)

    ;;highlight matching parenthesis
    (show-paren-mode 1)

    ;;insert matching delimiters
    (electric-pair-mode 1)

    ;;automatically reload files when they change on disk
    (global-auto-revert-mode 1)
    (setq auto-revert-check-vc-info t)

   ;;show column number in mode line
    (column-number-mode 1)

    ;;don't blink cursor
    (blink-cursor-mode 0)

    ;;adaptive cursor width shows width of character
    (setq x-stretch-cursor t)

    ;;show file-size in chars
    (size-indication-mode t)

    ;;change all yes/no questions to y/n type
    (fset 'yes-or-no-p 'y-or-n-p)

    ;;display time, date on mode-line
    (setq display-time-day-and-date t)
    (setq display-time-format "%H:%M %a %e %B %Y")
    (setq display-time-default-load-average nil)
    (display-time)

    ;;stop making backup(~) files
    ;;(setq make-backup-files nil)
    ;;(setq auto-save-default nil)
    ;;(setq create-lockfiles nil)
    (setq backup-directory-alist `(("." . ,(concat user-emacs-directory "backups"))))

    ;;set encoding to utf-8
    (set-language-environment "UTF-8")
    (set-default-coding-systems 'utf-8)

    ;;remove trailing whitespace before saving
    (add-hook 'before-save-hook 'delete-trailing-whitespace)

    ;;save clipboard string to kill-ring before replacing it
    (setq save-interprogram-paste-before-kill t)

    ;;keep track of window configurations
    (winner-mode t)

    ;;move directionally between neighbouring windows
    (windmove-default-keybindings)

     ;;sort results of apropos by relevancy
    (setq apropos-sort-by-scores t)

    ;;replace list-buffers with ibuffer(C-x C-b)
    (defalias 'list-buffers 'ibuffer)

    ;;disable tab indentation
    (setq-default indent-tabs-mode nil
	      tab-width 2)

    (setq compilation-scroll-output t)

    (delete-selection-mode 1)

    (define-fringe-bitmap 'tilde [0 0 0 113 219 142 0 0] nil nil 'center)
    (set-fringe-bitmap-face 'tilde 'font-lock-comment-face)
    (setcdr (assq 'empty-line fringe-indicator-alist) 'tilde)
    (setq-default indicate-empty-lines t)

    (set-face-attribute 'default nil :height 140)

    (setq save-interprogram-paste-before-kill t)

    ;; (setq load-prefer-newer t)

    (setq-default fill-column 80)

    (save-place-mode 1)
    (set-face-attribute 'default nil :height 140)
  #+END_SRC

** Set Default Browser
for MacOS
#+BEGIN_SRC emacs-lisp :results silent
(setq browse-url-browser-function 'browse-url-default-macosx-browser)
#+END_SRC

** Fancy Title-Bar
review this
Mac Specific
#+BEGIN_SRC emacs-lisp :results silent
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))
(setq ns-use-proxy-icon  nil)
(setq frame-title-format nil)
#+END_SRC

* Split windows Vertically
#+BEGIN_SRC emacs-lisp :results silent
  (setq split-height-threshold nil)
  (setq split-width-threshold 160)
#+END_SRC

* Auto Compile
#+BEGIN_SRC emacs-lisp :results silent
  (use-package auto-compile
    :config (auto-compile-on-load-mode))
#+END_SRC

* Snippets
** Smart Move Beginning of Line
   #+BEGIN_SRC emacs-lisp :results silent
  (defun smarter-move-beginning-of-line (arg)
    "Move point back to indentation of beginning of line.

  Move point to the first non-whitespace character on this line.
  If point is already there, move to the beginning of the line.
  Effectively toggle between the first non-whitespace character and
  the beginning of the line.

  If ARG is not nil or 1, move forward ARG - 1 lines first.  If
  point reaches the beginning or end of the buffer, stop there."
    (interactive "^p")
    (setq arg (or arg 1))

    ;; Move lines first
    (when (/= arg 1)
      (let ((line-move-visual nil))
  (forward-line (1- arg))))

    (let ((orig-point (point)))
      (back-to-indentation)
      (when (= orig-point (point))
  (move-beginning-of-line 1))))

  ;; remap C-a to `smarter-move-beginning-of-line'
  (global-set-key (kbd "C-a") 'smarter-move-beginning-of-line)
   #+END_SRC

** Copy file name to clipboard
   #+BEGIN_SRC emacs-lisp :results silent
(defun er-copy-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))
   #+END_SRC

** Open Current Directory in Finder
   #+BEGIN_SRC emacs-lisp :results silent
     (defun open-current-file-directory ()
       (interactive)
       (shell-command "open ."))
   #+END_SRC

** Toggle Window Split
#+BEGIN_SRC emacs-lisp :results silent
(defun toggle-window-split ()
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
         (next-win-buffer (window-buffer (next-window)))
         (this-win-edges (window-edges (selected-window)))
         (next-win-edges (window-edges (next-window)))
         (this-win-2nd (not (and (<= (car this-win-edges)
                     (car next-win-edges))
                     (<= (cadr this-win-edges)
                     (cadr next-win-edges)))))
         (splitter
          (if (= (car this-win-edges)
             (car (window-edges (next-window))))
          'split-window-horizontally
        'split-window-vertically)))
    (delete-other-windows)
    (let ((first-win (selected-window)))
      (funcall splitter)
      (if this-win-2nd (other-window 1))
      (set-window-buffer (selected-window) this-win-buffer)
      (set-window-buffer (next-window) next-win-buffer)
      (select-window first-win)
      (if this-win-2nd (other-window 1))))))
#+END_SRC
* Try
  to try out other packages
  #+BEGIN_SRC emacs-lisp :results silent
        (use-package try
          :defer 4)
  #+END_SRC

* Smooth Scrolling
#+BEGIN_SRC emacs-lisp :results silent
  (use-package smooth-scrolling
    :config
    (smooth-scrolling-mode 1))
#+END_SRC

* Diminish
#+BEGIN_SRC emacs-lisp :results silent
(use-package diminish
  :diminish (auto-revert-mode visual-line-mode))
#+END_SRC

* Org Mode
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package org :ensure org-plus-contrib :pin org)
    (global-set-key "\C-cl" 'org-store-link)
    (global-set-key "\C-ca" 'org-agenda)
    (global-set-key "\C-cc" 'org-capture)
    (global-set-key "\C-cb" 'org-switchb)
    (setq org-format-latex-header "\\documentclass{article}\n\\usepackage[usenames]{color}\n[PACKAGES]\n[DEFAULT-PACKAGES]\n\\pagestyle{empty}             % do not remove\n% The settings below are copied from fullpage.sty\n\\setlength{\\textwidth}{\\paperwidth}\n\\addtolength{\\textwidth}{-3cm}\n\\setlength{\\oddsidemargin}{1.5cm}\n\\addtolength{\\oddsidemargin}{-2.54cm}\n\\setlength{\\evensidemargin}{\\oddsidemargin}\n\\setlength{\\textheight}{\\paperheight}\n\\addtolength{\\textheight}{-\\headheight}\n\\addtolength{\\textheight}{-\\headsep}\n\\addtolength{\\textheight}{-\\footskip}\n\\addtolength{\\textheight}{-3cm}\n\\setlength{\\topmargin}{1.5cm}\n\\addtolength{\\topmargin}{-2.54cm}\\everymath{\\displaystyle}")
    (plist-put org-format-latex-options :scale 2.0)

    ;;let babel execute the following languages in org documents
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((python . t)
       (dot . t)
       (clojure .t)
       (shell . t)))

    ;;use org-bullets
    (use-package org-bullets
      :config (add-hook 'org-mode-hook 'org-bullets-mode))

  #+END_SRC

* Org Reveal
  #+BEGIN_SRC emacs-lisp :results silent
  (use-package ox-reveal
  :defer 6
  :ensure ox-reveal)

  (setq org-reveal-root "http://cdn.jsdelivr.net/reveal.js/3.0.0/")
  (setq org-reveal-mathjax t)

  (use-package htmlize)
  #+END_SRC

* Appearance
** Theme
   Monokai
   #+BEGIN_SRC emacs-lisp :results silent
     (use-package monokai-theme
       :init
       (load-theme 'monokai t))
   #+END_SRC

** Beacon
   #+BEGIN_SRC emacs-lisp :results silent
  (use-package beacon
    :config
    (setq beacon-mode 1))
   #+END_SRC

** Smart-Mode-Line
   #+BEGIN_SRC emacs-lisp :results silent
            (use-package smart-mode-line
              :init (add-hook 'after-init-hook 'sml/setup)
              (setq sml/theme 'respectful
                    sml/mode-width 'full
                    sml/namewidth 10))
   #+END_SRC

*** Powerline Theme
    #+BEGIN_SRC emacs-lisp :results silent
  (use-package smart-mode-line-powerline-theme
    :ensure t)
    #+END_SRC

* Which Key
  brings up help on key-combinations
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package which-key
    :config (which-key-mode))
  #+END_SRC

* Smex
  smart M-x enhancement.
  #+BEGIN_SRC emacs-lisp :results silent
  (use-package smex
    :config (smex-initialize))

  (global-set-key (kbd "M-x") 'smex)
  (global-set-key (kbd "M-X") 'smex-major-mode-commands)
  (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
  #+END_SRC

* Counsel
  collection of ivy-enchanced-versions of common emacs commands.
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package counsel
      :config (setq ivy-height 20)
      :bind (("M-y" . counsel-yank-pop)
      :map ivy-minibuffer-map
       ("M-y" . ivy-next-line)))
  #+END_SRC

* Swiper
  ivy: a generic completion mechanism for Emacs
  swiper: an Ivy-enhanced alternative to isearch.
  #+BEGIN_SRC emacs-lisp :results silent
            (use-package swiper
              :config (ivy-mode 1)
              (setq ivy-use-virtual-buffers t)
              (setq enable-recursive-minibuffers t)
              (setq ivy-count-format "(%d/%d) ")
              (global-set-key "\C-s" 'swiper)
              (global-set-key (kbd "C-c C-r") 'ivy-resume)
              (global-set-key (kbd "<f6>") 'ivy-resume)
              (global-set-key (kbd "M-x") 'counsel-M-x)
              (global-set-key (kbd "C-x C-f") 'counsel-find-file)
              (global-set-key (kbd "<f1> f") 'counsel-describe-function)
              (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
              (global-set-key (kbd "<f1> l") 'counsel-find-library)
              (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
              (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
              (global-set-key (kbd "C-c g") 'counsel-git)
              (global-set-key (kbd "C-c j") 'counsel-git-grep)
              (global-set-key (kbd "C-c k") 'counsel-ag)
              (global-set-key (kbd "C-x l") 'counsel-locate)
              (global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
              (define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)
              (setq ivy-re-builders-alist '((t . ivy--regex-fuzzy)))
              (setq ivy-on-del-error-function #'ignore))
  #+END_SRC

* Flx
Sort fuzzy-match results for Emacs a la Sublime Text.
#+BEGIN_SRC emacs-lisp :results silent
(use-package flx)
#+END_SRC
* Dired
  #+BEGIN_SRC emacs-lisp :results silent
      (use-package dired
        :ensure nil
        :config
        (when (string= system-type "darwin")
          (setq dired-use-ls-dired nil))
        (put 'dired-find-alternate-file 'disabled nil)
        (define-key dired-mode-map (kbd "RET") #'dired-find-alternate-file))
  #+END_SRC

* Company
  for auto-completion
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package company
      :bind (("C-c C-/" . company-complete))
      :config (global-company-mode)
      (setq company-idle-delay 0.2)
      (setq company-require-match nil)
      (setq company-selection-wrap-around t)
      (setq company-tooltip-align-annotations t)
      (setq company-tooltip-flip-when-above t)
      (setq company-transformers '(company-sort-by-occurrence))
      (define-key company-active-map (kbd "C-n") 'company-select-next)
      (define-key company-active-map (kbd "C-p") 'company-select-previous)
      (define-key company-search-map (kbd "C-n") 'company-select-next)
      (define-key company-search-map (kbd "C-p") 'company-select-previous)
      (define-key company-search-map (kbd "C-t") 'company-search-toggle-filtering))
  #+END_SRC

* Helpful
#+BEGIN_SRC emacs-lisp :results silent
  (use-package helpful
    :defer 5
    :config
    (global-set-key (kbd "C-h f") #'helpful-callable)
    (global-set-key (kbd "C-h v") #'helpful-variable)
    (global-set-key (kbd "C-h k") #'helpful-key)
    (global-set-key (kbd "C-c C-.") #'helpful-at-point))
#+END_SRC

* Undo Tree
  treats undo history as a tree.
  persists through sessions.
  #+BEGIN_SRC emacs-lisp :results silent
          (use-package undo-tree
            :diminish undo-tree-mode
            :bind
            ("C-z" . undo)
            ("C-S-z" . undo-tree-redo)
            :config
            (setq undo-tree-auto-save-history t
                  undo-tree-history-directory-alist `(("." . ,(concat user-emacs-directory "undo-tree-history/")))
                  undo-tree-visualizer-diff t
                  undo-tree-visualizer-timestamps t)
            :init
            (global-undo-tree-mode))
  #+END_SRC

* Dumb Jump
#+BEGIN_SRC emacs-lisp :results silent
  (use-package dumb-jump
    :ensure t
    :config (setq dumb-jump-prefer-searcher 'rg))

  (global-set-key (kbd "C-.") 'dumb-jump-go)
#+END_SRC
* Projectile
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package projectile
      :defer 1
      :config
      (projectile-global-mode)
      (setq projectile-completion-system 'ivy)
      (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

    (use-package counsel-projectile
      :defer 2
      :config
      (setq counsel-projectile-on t))
  #+END_SRC

* Grep
** ripgrep
   #+BEGIN_SRC emacs-lisp :results output silent
  (use-package ripgrep
    :ensure t)
   #+END_SRC
** projectile-ripgrep
   #+BEGIN_SRC emacs-lisp :results output silent
(use-package projectile-ripgrep
  :bind (("C-c g" . projectile-ripgrep)))
   #+END_SRC

** deadgrep
   #+BEGIN_SRC emacs-lisp :results output silent
  (use-package deadgrep
      :bind
      (("C-x C-g" . deadgrep)))
   #+END_SRC

* Rainbow Delimiters
   add colours to matching parens
   #+BEGIN_SRC emacs-lisp :results silent
     (use-package rainbow-delimiters
       :init (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
	     (add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode))
   #+END_SRC

* Anzu
for current match and total match information in the mode-line
#+BEGIN_SRC emacs-lisp :results silent
(use-package anzu
  :diminish (anzu-mode)
  :defer 5
  :config
  (global-set-key [remap query-replace] 'anzu-query-replace)
  (global-set-key [remap query-replace-regexp] 'anzu-query-replace-regexp)
  :init
  (global-anzu-mode t))
#+END_SRC

* Rest Client
  to replace postman
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package restclient
      :mode ("\\.rc\\'" . restclient-mode)
      :config
      (use-package company-restclient
	:ensure try
	:config
	(add-to-list 'company-backends 'company-restclient)))
  #+END_SRC

* Git Stuff
** Magit
  git interface
  #+BEGIN_SRC emacs-lisp :results output silent
        (use-package magit
          :bind ("C-x g" . magit-status)
           :config
        (setq magit-commit-arguments nil
              magit-diff-use-overlays nil
              magit-diff-refine-hunk t))

  #+END_SRC

** Git TimeMachine
#+BEGIN_SRC emacs-lisp :results silent
  (use-package git-timemachine)
#+END_SRC

** Git Gutter
#+BEGIN_SRC emacs-lisp :results silent
  (use-package git-gutter
    :defer 3
    :diminish (git-gutter-mode)
    :init (global-git-gutter-mode t)
    :config
    (setq git-gutter:added-sign "+"
    git-gutter:deleted-sign "-"
    git-gutter:modified-sign "~"
    git-gutter:update-interval 1)
    (set-face-foreground 'git-gutter:modified "#a36fff")
    (set-face-foreground 'git-gutter:added "#198844")
    (set-face-foreground 'git-gutter:deleted "#cc342b")
    (add-to-list 'git-gutter:update-hooks 'focus-in-hook)
    (add-hook 'git-gutter:update-hooks 'magit-after-revert-hook)
    (add-hook 'git-gutter:update-hooks 'magit-not-reverted-hook))
#+END_SRC

* Keychain Environment
#+BEGIN_SRC emacs-lisp :results silent
  (use-package keychain-environment
    :config
    (keychain-refresh-environment))

#+END_SRC

* Hydra
#+BEGIN_SRC emacs-lisp :results silent
  (use-package hydra
    :defer t)
#+END_SRC

* Flycheck
#+BEGIN_SRC emacs-lisp :results silent
  (use-package flycheck
    :defer 5
      :config
      (define-key flycheck-mode-map flycheck-keymap-prefix nil)
      (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))
      (setq flycheck-idle-change-delay 3.0)
      (define-key flycheck-mode-map flycheck-keymap-prefix flycheck-command-map)
      :init
      (global-flycheck-mode))

  ;; (defhydra hydra-flycheck (:hint nil))
  ;; (defhydra hydra-flycheck
  ;;   (:pre (progn (setq hydra-lv t) (flycheck-list-errors))
  ;;    :post (progn (setq hydra-lv nil) (quit-windows-on "*Flycheck errors*"))
  ;;    :hint nil)
  ;;   "Errors"
  ;;   ("f"  flycheck-error-list-set-filter                            "Filter")
  ;;   ("j"  flycheck-next-error                                       "Next")
  ;;   ("k"  flycheck-previous-error                                   "Previous")
  ;;   ("gg" flycheck-first-error                                      "First")
  ;;   ("G"  (progn (goto-char (point-max)) (flycheck-previous-error)) "Last")
  ;;   ("q"  nil))

  ;; (bind-key "C-c f" 'hydra-flycheck/body)
#+END_SRC

* Free keys
#+BEGIN_SRC emacs-lisp :results output silent
(use-package free-keys
    :ensure t)
#+END_SRC

* Google-this
#+BEGIN_SRC emacs-lisp :results output silent
  (use-package google-this
    :diminish (google-this-mode)
    :bind
    ("C-c /" . google-this-mode-submap)
    :config
    (google-this-mode 1))
#+END_SRC

* All the Icons
#+BEGIN_SRC emacs-lisp :results silent
  (use-package all-the-icons
    :ensure t)
#+END_SRC

* Key Frequency
#+BEGIN_SRC emacs-lisp :results output silent
  (use-package keyfreq
    :config
    (keyfreq-mode t)
    (keyfreq-autosave-mode t))
#+END_SRC

* Uniquify
#+BEGIN_SRC emacs-lisp :results silent
  (use-package uniquify
    :defer 3
    :ensure nil
    :custom
    (uniquify-after-kill-buffer-p t)
    (uniquify-buffer-name-style 'post-forward)
    (uniquify-strip-common-suffix t))
#+END_SRC

* Multiple Cursors
basic keybindings
#+BEGIN_SRC emacs-lisp :results silent
    (use-package multiple-cursors
      :bind (("C->" . mc/mark-next-like-this)
             ("C-<" . mc/mark-previous-like-this)
             ("C-c C-a" . mc/mark-all-like-this)
             ("C-c C-l" . mc/edit-lines))
      :init (multiple-cursors-mode))
#+END_SRC

keybindings with hydra
#+BEGIN_SRC emacs-lisp :results silent
  ;; (use-package multiple-cursors
  ;;   :commands (mc/add-cursor-on-click
  ;;              mc/edit-beginning-of-lines
  ;;              mc/edit-lines
  ;;              mc/insert-numbers
  ;;              mc/qmark-all-dwim
  ;;              mc/mark-all-in-region-regexp
  ;;              mc/mark-all-likey-this
  ;;              mc/mark-next-like-this
  ;;              mc/mark-previous-like-this
  ;;              mc/mark-sgml-tag-pair
  ;;              mc/reverse-regions
  ;;              mc/skip-to-next-like-this
  ;;              mc/skip-to-previous-like-this
  ;;              mc/sort-regions
  ;;              mc/unmark-next-like-this
  ;;              mc/unmark-previous-like-this)
  ;;   )

  ;; (defhydra hydra-mc (:hint nil)
  ;;   "
  ;;       ^Up^            ^Down^        ^All^                ^Lines^               ^Edit^                 ^Other^
  ;; ----------------------------------------------------------------------------------------------------
  ;; [_p_]   Next    [_n_]   Next    [_a_] All like this  [_l_] Edit lines      [_i_] Insert numbers   [_t_] Tag pair
  ;; [_P_]   Skip    [_N_]   Skip    [_r_] All by regexp  [_L_] Edit line beg.  [_s_] Sort regions      ^ ^
  ;; [_M-p_] Unmark  [_M-n_] Unmark  [_d_] All DWIM        ^ ^                  [_R_] Reverse regions  [_q_] Quit
  ;; "
  ;;   ("p" mc/mark-previous-like-this)
  ;;   ("P" mc/skip-to-previous-like-this)
  ;;   ("M-p" mc/unmark-previous-like-this)

  ;;   ("n" mc/mark-next-like-this)
  ;;   ("N" mc/skip-to-next-like-this)
  ;;   ("M-n" mc/unmark-next-like-this)

  ;;   ("a" mc/mark-all-like-this :exit t)
  ;;   ("r" mc/mark-all-in-region-regexp :exit t)
  ;;   ("d" mc/mark-all-dwim :exit t)

  ;;   ("l" mc/edit-lines :exit t)
  ;;   ("L" mc/edit-beginnings-of-lines :exit t)

  ;;   ("i" mc/insert-numbers)
  ;;   ("s" mc/sort-regions)
  ;;   ("R" mc/reverse-regions)

  ;;   ("t" mc/mark-sgml-tag-pair)
  ;;   ("q" nil)

  ;;   ("<mouse-1>" mc/add-cursor-on-click)
  ;;   ("<down-mouse-1>" ignore)
  ;;   ("<drag-mouse-1>" ignore))

  ;; (bind-key "C-c m" 'hydra-mc/body)
#+END_SRC

* Paradox
#+BEGIN_SRC emacs-lisp :results silent :tangle no
  (use-package paradox
    :config
    (setq paradox-execute-asynchronously t))
#+END_SRC

* Neotree
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package neotree
      :config
      (setq neo-window-width 50)
      (setq neo-theme 'nerd)
      (customize-set-variable 'neo-theme (if (display-graphic-p) 'icons 'arrow))
      (customize-set-variable 'neo-smart-open t)
      (customize-set-variable 'projectile-switch-project-action 'neotree-projectile-action)
      (defun neotree-project-dir ()
        "Open NeoTree using the git root."
        (interactive)
        (let ((project-dir (projectile-project-root))
              (file-name (buffer-file-name)))
          (neotree-toggle)
          (if project-dir
              (if (neo-global--window-exists-p)
                  (progn
                    (neotree-dir project-dir)
                    (neotree-find file-name)))
            (message "Could not find git project root."))))
      :bind
      ([f8] . neotree-project-dir))

    ;; Disable line-numbers minor mode for neotree
    (add-hook 'neo-after-create-hook (lambda (&optional dummy) (display-line-numbers-mode -1)))
  #+END_SRC

* Markdown Mode
#+BEGIN_SRC emacs-lisp :results silent
  (use-package markdown-mode
    :commands (markdown-mode gfm-mode)
    :mode ("\\.md\\'" . gfm-mode)
    :init
    (setq markdown-command "multimarkdown"))
#+END_SRC

* Rainbow Mode
#+BEGIN_SRC emacs-lisp :results silent
  (use-package rainbow-mode
    :diminish rainbow-mode
    :init
    (add-hook 'prog-mode-hook 'rainbow-mode))
#+END_SRC

* JSON
#+BEGIN_SRC emacs-lisp :results silent
 (use-package json-mode
   :ensure t
   :mode (("\\.json\\'" . json-mode)
       ("\\.tmpl\\'" . json-mode)
       ("\\.eslintrc\\'" . json-mode))
   :config (setq-default js-indent-level 2))

   (use-package json-reformat
     :ensure t
     :after json-mode
     :bind (("C-c r" . json-reformat-region)))

 (defun json-minify-buffer-contents()
   "Minifies the buffer contents by removing whitespaces."
   (interactive)
   (delete-whitespace-rectangle (point-min) (point-max))
   (mark-whole-buffer)
   (goto-char (point-min))
   (while (search-forward "\n" nil t) (replace-match "" nil t)))
#+END_SRC

* Tramp
#+BEGIN_SRC emacs-lisp :results silent
  (use-package tramp
    :defer 4
    :config
        (setq tramp-default-method "ssh"
              ido-enable-tramp-completion t
              tramp-backup-directory-alist backup-directory-alist
              auto-save-file-name-transforms nil)
        (tramp-set-completion-function "ssh"
                                       '((tramp-parse-sconfig "~/.ssh/config"))))
#+END_SRC

* Expand Region
#+BEGIN_SRC emacs-lisp :results silent
  (use-package expand-region
    :config
    (global-set-key (kbd "C-=") 'er/expand-region))
#+END_SRC

* Web Mode

  #+BEGIN_SRC emacs-lisp :results silent
    ;; for html & css
    (use-package web-mode
      :defer t
      :init (add-hook 'before-save-hook 'web-mode-buffer-indent)    ;;indent buffer before saving
      :mode
      ("\\.html?\\'" . web-mode)
      ("\\.css?\\'" . web-mode)
      :config
      (setq-default indent-tabs-mode nil)
      (setq web-mode-markup-indent-offset 2)
      (setq web-mode-code-indent-offset 2)
      (setq web-mode-css-indent-offset 2)
      (setq web-mode-script-padding 0)
      (setq web-mode-enable-auto-expanding t)
      (setq web-mode-enable-css-colorization t)
      (setq web-mode-enable-auto-pairing nil)
      (setq web-mode-enable-auto-closing t)
      (setq web-mode-enable-auto-quoting t)
      (setq web-mode-auto-close-style 2)      ;;close after opening-tag
      (setq web-mode-auto-quote-style 2))     ;;use single-quotes for attributes(requires v15)
  #+END_SRC

* Emmet Mode
  produces HTML from CSS-like selectors
  #+BEGIN_SRC emacs-lisp :results silent
  (use-package emmet-mode
    :config
    (add-hook 'sgml-mode-hook 'emmet-mode)
    (add-hook 'web-mode-hook 'emmet-mode)
    (add-hook 'css-mode-hook 'emmet-mode))
  #+END_SRC

* Yasnippet
  allows to expand text aliases
  #+BEGIN_SRC emacs-lisp :results silent
    (use-package yasnippet
     :init (yas-global-mode 1))
  #+END_SRC

* Dumb Jump
#+BEGIN_SRC emacs-lisp  :results silent
  (use-package dumb-jump
    :init
    (bind-key* "M-g f" 'dumb-jump-go)
    (bind-key* "M-g b" 'dumb-jump-back))
#+END_SRC

* Some missing stuff from previous config
  #+BEGIN_SRC emacs-lisp :results silent
    (defun indent-region-or-buffer ()
      "Indent a region if selected, otherwise the whole buffer."
      (interactive)
      (save-excursion
        (if (region-active-p)
      (progn (indent-region (region-beginning) (region-end)))
          (progn (indent-buffer)))))

    (defun move-line-up ()
      (interactive)
      (transpose-lines 1)
      (forward-line -2))

    (defun move-line-down ()
      (interactive)
      (forward-line 1)
      (transpose-lines 1)
      (forward-line -1))

    (defun kill-other-buffers ()
      "Kill all other buffers."
      (interactive)
      (mapc 'kill-buffer
            (delq (current-buffer)
                  (remove-if-not 'buffer-file-name (buffer-list)))))

      (global-set-key (kbd "s-t") 'projectile-find-file)
      (global-set-key (kbd "s-g") 'projectile-grep)
      (global-set-key (kbd "s-p") 'projectile-switch-project)
      (global-set-key (kbd "M-S-<up>") 'move-line-up)
      (global-set-key (kbd "M-S-<down>") 'move-line-down)
      (global-set-key (kbd "C-c n") 'indent-region-or-buffer)
      (global-set-key (kbd "s-f") 'swiper)

      (setq
    frame-title-format
    '((:eval (if (buffer-file-name)
           (abbreviate-file-name (buffer-file-name))
         "%b"))))

    ;;from  https://github.com/gja/dotemacs/blob/master/modules/sublime.el

    (defmacro move-back-horizontal-after (&rest code)
      `(let ((horizontal-position (current-column)))
         (progn
           ,@code
           (move-to-column horizontal-position))))

    (defun comment-or-uncomment-line-or-region ()
      (interactive)
      (if (region-active-p)
        (comment-or-uncomment-region (region-beginning) (region-end))
        (move-back-horizontal-after
         (comment-or-uncomment-region (line-beginning-position) (line-end-position))
         (forward-line 1))))

    (defun duplicate-line ()
      (interactive)
      (move-back-horizontal-after
       (move-beginning-of-line 1)
       (kill-line)
       (yank)
       (open-line 1)
       (forward-line 1)
       (yank)))

    (defun expand-to-word-and-multiple-cursors (args)
      (interactive "p")
      (if (region-active-p) (mc/mark-next-like-this args) (er/mark-word)))

    (global-set-key (kbd "M-s-<right>") 'switch-to-next-buffer)
    (global-set-key (kbd "M-s-<left>") 'switch-to-prev-buffer)
    (global-set-key (kbd "s-D") 'duplicate-line)
    (global-set-key (kbd "s-Z") 'undo-tree-redo)
    (global-set-key (kbd "M-;") 'comment-or-uncomment-line-or-region)
    (global-set-key (kbd "s-d") 'expand-to-word-and-multiple-cursors)
  #+END_SRC

